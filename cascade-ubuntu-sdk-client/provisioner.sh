#!/bin/bash

echo "START PROVISIONING --------------------------------------------"

export DEBIAN_FRONTEND=noninteractive

echo "Ensure external repositiories ---------------------------------"
add-apt-repository ppa:nextcloud-devs/client

echo "Upgrade packages ----------------------------------------------"
apt-get update -y
apt-get upgrade -y

echo "Adjust partition and filesystem -------------------------------"
apt-get install -y cloud-guest-utils
growpart /dev/sda 1
resize2fs /dev/sda1

echo "Ensure ansible provisioner ------------------------------------"
apt-get install -y ansible ansible-lint libfontconfig1 mesa-common-dev 
apt-get install -y libgl1-mesa-dev libxkbcommon-x11-0 libxcb-xinerama0 libglu1-mesa-dev 

echo "Ensure base packages ------------------------------------------"
apt-get install -y gnome-tweaks gnome-system-tools gnome-shell-extension-arc-menu 
apt-get install -y nautilus-gtkhash nautilus-admin nautilus-actions rabbitvcs-core
apt-get install -y nautilus-nextcloud nautilus-image-converter rabbitvcs-nautilus
apt-get install -y build-essential software-properties-common wget curl git
apt-get install -y clang clang-format clang-tidy clang-tools clang-tidy clazy lldb lld 
apt-get install -y libc++-dev libc++1 libc++abi-dev libc++abi1 libclang-dev libclang1 
apt-get install -y python3-pip python3-dev python3-testresources python-clang 
apt-get install -y valgrind 

echo "Ensure multimedia packages ------------------------------------"
apt-get install -y ubuntu-restricted-extras

echo "Ensure packages needed by Android Studio ----------------------"
apt-get install -y libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386

echo "Remove wrong packages -----------------------------------------"
apt-get purge -y nautilus-owncloud 

echo "Install ansible galaxy roles and collections-------------------"
ansible-galaxy collection install -r /opt/provisioning/requirements.yml
ansible-galaxy role install -r /opt/provisioning/requirements.yml

echo "Execute ansible playbooks -------------------------------------"
export ANSIBLE_LOCALHOST_WARNING=0
export ANSIBLE_CALLBACK_WHITELIST=profile_tasks                            
ansible-playbook /opt/provisioning/playbook-user-root.yml
ansible-playbook /opt/provisioning/playbook-user-vagrant.yml
ansible-playbook /opt/provisioning/playbook-user-dev.yml

echo "Execute user-defined provisioner ------------------------------"
if [ -f /home/dev/Provisioning/provisioner.sh ]
then
  sudo /home/dev/Provisioning/provisioner.sh
else   
  echo "  --> no user-defined provisioner (/home/dev/Provisioning/provisioner.sh) found." 
fi  

echo "Cleanup -------------------------------------------------------"
apt-get autoclean -y  
apt-get autoremove -y

echo "PROVISIONING DONE"

