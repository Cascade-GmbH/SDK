# Cascade Ubuntu SDK Client

**Version: 0.9.5 beta**¹

- Targets: Linux Desktop, WebAssembly, Android
- Features: Qt 5.15.2, Qt 6.20 preview, GCC & Clang C++, Python 3

Known Issues to be fixed in a future release:
- Automatic provisioning not yet enabled
- WebAssembly not yet enabled for QtCreator (configuration missing)
- Android not yet enabled for QtCreator (Android Studio missing)
- Installation on Windows needs anti-virus software to be switched-off

**Attention: Never ever delete the local git-directory!!**  
It contains also the vagrant project (in .vagrant subdirectory) for the virtual machine; you risk to lose the provisioning-ability! If this happened anyway, try to follow [these instructions](https://subscription.packtpub.com/book/virtualization-and-cloud/9781784393748/1/ch01lvl1sec14/using-existing-virtual-machines-with-vagrant) (not verified!).

## Installation Instructions

- Install [Vagrant](https://www.vagrantup.com/) and [VirtualBox](https://www.virtualbox.org/) (or any other [supported provider](https://www.vagrantup.com/docs/providers)) on your Linux/Windows/MacOS host machine

- Clone the Cascade SDK repository (this) to your host machine
  
- Open a terminal on the host and cd into directory cascade-ubuntu-sdk-client (this)
- Copy a valid qtaccount.ini file into this directory (don't worry: you cannot push and it's also in .gitignore) 
- *$ vagrant up*

*Don't touch the upcoming virtual machine until the process in the terminal has ended!* 

Please be patient, calculate one hour for the whole construction. After 'VIRTUAL MACHINE READY' you can login as user: 

- dev  
  initial password 'dev'

- vagrant  
  initial password 'vagrant';  
  this is a system account, you should normally not need it

Please change both passwords immediately!

## Knowledge-Base

##### How to configure QtCreator after first startup

Open menu *Options/Devices*: 
- Tab *WebAssembly*: insert Emscripten SDK Path "/opt/emsdk/" (not yet working!)
- Tab *Android*: click button "'Set Up SDK" and follow the instructions (not yet working!)

##### How to apply new provisioning-versions (manually)

First close all open documents within your VM (because it could be reset) and take a snapshot to be able to return to a working state in case of any error.

Then on your host machine:
- Open a terminal on the host and cd into directory cascade-ubuntu-sdk-client (this)
- $ git pull
- $ vagrant provision

*Don't touch the upcoming virtual machine until the process in the terminal has ended!*  

##### How to add additional software

Install whatever you want using also the Ubuntu package manager. But we suggest you to do this always via a user-defined provisioner inside the VM  (see next how to), so that your VM becomes (with a simple backup of /home/dev/) totally reproducible. And if you think that the software should be installed on all Cascade Ubuntu SDK Clients: let us know on the SDK's [project board](https://gitlab.com/Cascade-GmbH/SDK/-/boards).

##### How to add a user-defined provisioner inside the VM

- Edit the provisioner shell-script inside the VM:
  /home/dev/Provioning/provisioner.sh
- Open a terminal on the host and cd into directory cascade-ubuntu-sdk-client (this)
- $ vagrant provision

*Don't touch the upcoming virtual machine until the process in the terminal has ended!*

##### How to increase the VM disk size

Attention: Once increased, it is not possible to shrink the disk size afterwards! Any attempt to decrease it will be silently ignored. It is also always a good idea to make a VM snapshot before touching the partition table. That said:

- Make sure that the VM is powered off. 

Then on your host machine:
- Simply increase the virtual machine's disk using the Virtual Media Manager in VirtualBox
- Open a terminal on the host and cd into directory cascade-ubuntu-sdk-client (this)
- $ vagrant up --provision

*Don't touch the upcoming virtual machine until the process in the terminal has ended!*

##### How to (re-)associate an existing VirtualBox to an existing Vagrant project

This is a way to (re-)associate a VirtualBox VM if the Vagrant project somehow has disassociated.  

- Make sure that the VM is powered off. 

Then on your host machine:

- Open a terminal on the host and cd into directory cascade-ubuntu-sdk-client (this)
- $ VBoxManage list vms

You will get a list of all currently installed VM's, together with their ID's {xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx}. Identify the VM you want to associate and copy its ID. Then Create a file called id with the ID of your VM (paste the ID in the following command):

- $ echo -n 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx' > .vagrant/machines/default/virtualbox/id
- $ vagrant up 

*Don't touch the upcoming virtual machine until the process in the terminal has ended!*

##### How to improve vm-velocity on Ubuntu hosts

*$ sudo apt-get install bridge-utils qemu-kvm vagrant-libvirt libvirt-daemon-system libvirt-clients*

## References

¹) Versioning following the [semantic versioning spec](https://semver.org/spec/v2.0.0.html) v2.0.0.
