# Cascade SDK

This is a unified reproducible development environment, based on virtual machines that are automatically provisioned and updated. All you need is to install [VirtualBox](https://www.virtualbox.org/) and [Vagrant](https://www.vagrantup.com/) (or any other [supported provider](https://www.vagrantup.com/docs/providers)) on a Linux/Windows/MacOS host and to checkout this repository. 

Then follow the instructions in the folder of the client-machine(s) you want to use:

- [Cascade Ubuntu SDK Client](cascade-ubuntu-sdk-client)  
  current targets: Linux Desktop, WebAssembly  
  planned targets: Windows Desktop, Android, Raspberry OS, Embedded

- [Cascade macOs SDK Client](cascade-macos-sdk-client)  
  planned targets: macOs, iOS

